import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { CardEnterpriseComponent } from './pages/card-enterprise/card-enterprise.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'card-enterprise', component: CardEnterpriseComponent },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule { }
