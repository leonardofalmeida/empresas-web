import { Injectable } from '@angular/core';
import { RequestOptions, Response } from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

    API_URL = 'http://empresas.ioasys.com.br/';
    TOKEN_KEY = 'token';
    accessToken;
    uid;
    client;

    constructor(private http: HttpClient, private router: Router) { }

    get token() {
        return localStorage.getItem(this.TOKEN_KEY);
    }

    get isAuthenticated() {
        return !!localStorage.getItem(this.TOKEN_KEY);
    }

    logout() {
        localStorage.removeItem(this.TOKEN_KEY);
        this.router.navigateByUrl('/');
    }

    login(email: string, pass: string) {
        const headers = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-cache' })
        };

        const data = {
            email: email,
            password: pass
        };

        this.http.post(this.API_URL + '/api/v1/users/auth/sign_in', data, headers).subscribe(
            (res: any) => {
                
                localStorage.setItem(this.accessToken, res.access_token),
                localStorage.setItem(this.client, res.client),
                localStorage.setItem(this.uid, res.uid),
                this.router.navigateByUrl('/home');
            }
        );
    }

    getAccount() {
        return this.http.get(this.API_URL + '/account');
    }

    list() {
      const headers = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'JWT ' + this.TOKEN_KEY,
          'access-token': localStorage.getItem('accessToken'),
          'client': localStorage.getItem('client'),
          'uid':localStorage.getItem('uid'),   // this is our token from the UserService (see Part 1)
        })
      };

      return this.http.get('http://empresas.ioasys.com.br/api/v1/enterprises/1', headers);
    }
}