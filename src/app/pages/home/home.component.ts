import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public router: Router, private _userService: UserService) { }

  public posts;
  public new_post: any;

  ngOnInit() {
    this.getPosts();
    this.new_post = {};
  }

  getPosts() {
    this._userService.list().subscribe(
      // the first argument is a function which runs on success
      data => {
        this.posts = data;
        console.log(this.posts);
      },
      // the second argument is a function which runs on error
      err => console.error(err),
      // the third argument is a function which runs on completion
      () => {
        console.log('done loading posts')
        console.log(this.posts);
      }
    );
  }

}
