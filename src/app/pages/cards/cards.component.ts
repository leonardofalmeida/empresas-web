import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  constructor(private router: Router ) {}

  viewCard(){
    this.router.navigateByUrl('/card-enterprise');
  }

  ngOnInit() {
  }

}
